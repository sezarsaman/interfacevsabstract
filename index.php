<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
spl_autoload_register('autoload');
function autoload($className)
{
    $classFilePath = str_replace('\\',DIRECTORY_SEPARATOR,$className);
    $classFilePath.='.php';
    if(file_exists($classFilePath) && is_readable($classFilePath))
    {
        include_once $classFilePath;
    }
}

$special = new \classes\special();
$price = $special->getPostPrice('Milan');
echo "The price is $price <br />";

$pishtaz = new \classes\pishtaz();
$price = $pishtaz->getPostPrice('Tehran');
echo "The price is $price <br />";

$tipax = new \classes\tipax();
$price = $tipax->getPostPrice('Paris');
echo "The price is $price <br />";