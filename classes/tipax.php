<?php
namespace classes;

use classes\protocols\methods;

use classes\protocols\tax;

class tipax extends methods implements tax
{
    public function getPostPrice($location)
    {
        switch ($location){
            case 'Tehran':
                $price = 1 + (1*$this->addTax($location));
                break;
            case 'Berlin':
                $price = 10 + (10*$this->addTax($location));
                break;
            case 'Paris':
                $price = 100 + (100*$this->addTax($location));
                break;
            case 'Milan':
                $price = 1000 + (1000*$this->addTax($location));
                break;
            default:
                $price = 0;
        } 
        return $price; 
    }
    public function addTax($location){
        switch ($location){
            case 'Tehran':
                $tax = 0.1;
                break;
            case 'Berlin':
                $tax = 0.2;
                break;
            case 'Paris':
                $tax = 0.3;
                break;
            case 'Milan':
                $tax = 0.4;
                break;
            default:
                $tax = 0;
        } 
        return $tax;
    }
}